.POSIX:

all: getconf/getconf hostname/hostname localedef/localedef

getconf/getconf:
	cd getconf && $(MAKE)

hostname/hostname:
	cd hostname && $(MAKE)

localedef/localedef:
	cd localedef && ${MAKE}

clean:
	cd getconf && $(MAKE) clean
	cd hostname && $(MAKE) clean
	cd localedef && $(MAKE) clean

install:
	mkdir -p $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/usr/share/man/man1
	cd getconf && $(MAKE) install
	cd hostname && $(MAKE) install
	cd localedef && $(MAKE) install
