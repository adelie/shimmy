===============================
 Contribution Guide for Shimmy
===============================
:Author:
  * **A. Wilcox**, documentation writer
:Status:
  Draft
:Copyright:
  © 2016-2022 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains code and documentation for Shimmy, a small set of
utilities designed to bring stricter POSIX® compliance to open-source operating
environments.  It is designed mainly for use on Adélie Linux, but it is also
occasionally tested on Gentoo Linux, FreeBSD, OpenBSD, and Mac OS, among others.


License
```````

As the Adélie Linux project is an open-source Linux distribution, this package
is distributed under the same NCSA open source license as the distribution.


Changes
```````

Any changes to this repository must be reviewed before being pushed to the
current branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Adding a New Utility
====================

The addition of more compliant utilities is always appreciated.  If you would
like to contribute one, please ensure that:

* the utility in question is specified in IEEE Std 1003.1, 2013 Edition
  (Issue 7), or Linux Standards Base Core Specification, 5.0.0;

* you have created a new directory in the top level named after the utility;

* you have included an appropriate man page for it in the directory;

* and it compiles cleanly on a strictly compliant POSIX® environment.

Note that strictly compliant POSIX® software may include shims to compile on
non-compliant systems as well - the only requirement is that they *can* be
compiled on strictly compliant systems with no changes, and that they do not
rely on unspecified interfaces or return values.




Testing
=======

You **must** test any changes on Adélie Linux.  It is recommended that you
additionally test your changes on at least a glibc-based Linux distribution.

You are welcome to test on a Berkeley Software Distribution.  Please note
however that they are not POSIX® compliant and may require some changes to
ensure build success.

You are additionally welcome to test on Mac OS X, Solaris, AIX, and HP-UX.
However, some of these systems deviate from POSIX® in some APIs or do not fully
implement every option.  Care should be taken when testing on these systems.




Extensions
==========

Some extensions, such as BSD or GNU, can be useful in some situations.  We will
review extensions on a case-by-case basis.  Please do note that extensions that
directly conflict with the POSIX® specification (that is, where implementation
of said extension would cause loss of compliance with POSIX®) will never be
accepted in tree.




Contributing Changes
====================

This section describes the usual flows of contribution to this repository.


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the packages repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.


Mailing List
````````````

#. Clone the packages repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit``.

#. Use the command ``git format-patch HEAD^`` to create a patch file for your
   commit.

   .. note:: If you have made multiple commits to the tree, you will need to
             add an additional ^ for each commit you have made.  For example,
             if you have made three commits, you will use the command
             ``git format-patch HEAD^^^``.

#. Email the resulting patch to the packagers mailing list.




Legal Notice
============

POSIX® is a registered trademark of the IEEE.  While we aim to provide utilities
that meet the POSIX® specification, this package has not yet been submitted to
the Open Group for testing, and thus cannot be termed as "POSIX® compliant" in
any documentation.  Instead, use "POSIX® conformant".
