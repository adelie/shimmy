/*
 * getconf - get important system configuration values from the shell.
 * Copyright (c) 2016-2024 Adélie Linux Team.  All rights reserved.
 * Licensed under the NCSA open source license.
 * See LICENSE file included with this source for more information.
 */


#ifdef __linux__
#	include <sys/statvfs.h>	/* statvfs */
#	include <sys/vfs.h>	/* statfs */
#endif
#include <errno.h>	/* errno */
#include <limits.h>	/* limit_vars */
#include <stdbool.h>	/* bool type and true/false */
#include <stdio.h>	/* stderr, fprintf */
#include <stdlib.h>	/* malloc, free */
#include <string.h>	/* strlen */
#include <strings.h>	/* strcasecmp */
#include <unistd.h>	/* sysconf + _SC friends, confstr + _CS friends */

typedef struct variable_mapping
{
	const char *variable;
	const int value;
} var_map_t;

/* For variables that are valid, but undefined in this implementation */
#define _UNDEFINED_VARIABLE_ 0x7FEEDFEE

#define SHIMMY_POSIX2_LINE_MAX 0x7FEEDFED
#define SHIMMY_INT_MAX 0x7FEEDFEC
#define SHIMMY_LONG_BIT 0x7FEEDFEB

const var_map_t sysconf_vars[] = {
	/* max */
	{ "AIO_LISTIO_MAX", _SC_AIO_LISTIO_MAX },
	{ "AIO_MAX", _SC_AIO_MAX },
#if defined(_SC_AIO_PRIO_DELTA_MAX)
	{ "AIO_PRIO_DELTA_MAX", _SC_AIO_PRIO_DELTA_MAX },
#else /* NetBSD */
	{ "AIO_PRIO_DELTA_MAX", _UNDEFINED_VARIABLE_ },
#endif
	{ "ARG_MAX", _SC_ARG_MAX },
	{ "ATEXIT_MAX", _SC_ATEXIT_MAX },
	{ "BC_BASE_MAX", _SC_BC_BASE_MAX },
	{ "BC_DIM_MAX", _SC_BC_DIM_MAX },
	{ "BC_SCALE_MAX", _SC_BC_SCALE_MAX },
	{ "BC_STRING_MAX", _SC_BC_STRING_MAX },
	{ "CHILD_MAX", _SC_CHILD_MAX },
	{ "COLL_WEIGHTS_MAX", _SC_COLL_WEIGHTS_MAX },
#if defined(_SC_DELAYTIMER_MAX)
	{ "DELAYTIMER_MAX", _SC_DELAYTIMER_MAX },
#else /* NetBSD */
	{ "DELAYTIMER_MAX", _UNDEFINED_VARIABLE_ },
#endif
	{ "EXPR_NEST_MAX", _SC_EXPR_NEST_MAX },
	{ "HOST_NAME_MAX", _SC_HOST_NAME_MAX },
	{ "IOV_MAX", _SC_IOV_MAX },
	{ "LINE_MAX", _SC_LINE_MAX },
	{ "LOGIN_NAME_MAX", _SC_LOGIN_NAME_MAX },
	{ "NGROUPS_MAX", _SC_NGROUPS_MAX },
	{ "MQ_OPEN_MAX", _SC_MQ_OPEN_MAX },
	{ "MQ_PRIO_MAX", _SC_MQ_PRIO_MAX },
	{ "OPEN_MAX", _SC_OPEN_MAX },
	{ "PAGE_SIZE", _SC_PAGE_SIZE },
	{ "PAGESIZE", _SC_PAGESIZE },
	{ "PTHREAD_DESTRUCTOR_ITERATIONS", _SC_THREAD_DESTRUCTOR_ITERATIONS },
	{ "PTHREAD_KEYS_MAX", _SC_THREAD_KEYS_MAX },
	{ "PTHREAD_STACK_MIN", _SC_THREAD_STACK_MIN },
	{ "PTHREAD_THREADS_MAX", _SC_THREAD_THREADS_MAX },
	{ "RE_DUP_MAX", _SC_RE_DUP_MAX },
#if defined(_SC_RTSIG_MAX)
	{ "RTSIG_MAX", _SC_RTSIG_MAX },
#else /* NetBSD */
	{ "RTSIG_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_SEM_NSEMS_MAX)
	{ "SEM_NSEMS_MAX", _SC_SEM_NSEMS_MAX },
#else /* NetBSD */
	{ "SEM_NSEMS_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_SEM_VALUE_MAX)
	{ "SEM_VALUE_MAX", _SC_SEM_VALUE_MAX },
#else /* NetBSD */
	{ "SEM_VALUE_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_SIGQUEUE_MAX)
	{ "SIGQUEUE_MAX", _SC_SIGQUEUE_MAX },
#else /* NetBSD */
	{ "SIGQUEUE_MAX", _UNDEFINED_VARIABLE_ },
#endif
	{ "STREAM_MAX", _SC_STREAM_MAX },
	{ "SYMLOOP_MAX", _SC_SYMLOOP_MAX },
#if defined(_SC_TIMER_MAX)
	{ "TIMER_MAX", _SC_TIMER_MAX },
#else /* NetBSD */
	{ "TIMER_MAX", _UNDEFINED_VARIABLE_ },
#endif
	{ "TTY_NAME_MAX", _SC_TTY_NAME_MAX },
	{ "TZNAME_MAX", _SC_TZNAME_MAX },

	/* POSIX */
#if defined(_SC_ADVISORY_INFO)
	{ "_POSIX_ADVISORY_INFO", _SC_ADVISORY_INFO },
#else /* NetBSD */
	{ "_POSIX_ADVISORY_INFO", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_BARRIERS", _SC_BARRIERS },
	{ "_POSIX_ASYNCHRONOUS_IO", _SC_ASYNCHRONOUS_IO },
	{ "_POSIX_CLOCK_SELECTION", _SC_CLOCK_SELECTION },
#if defined(_SC_CPUTIME)
	{ "_POSIX_CPUTIME", _SC_CPUTIME },
#else /* NetBSD */
	{ "_POSIX_CPUTIME", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_FSYNC", _SC_FSYNC },
#if defined(_SC_IPV6)
	{ "_POSIX_IPV6", _SC_IPV6 },
#else /* NetBSD */
	{ "_POSIX_IPV6", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_JOB_CONTROL", _SC_JOB_CONTROL },
	{ "_POSIX_MAPPED_FILES", _SC_MAPPED_FILES },
	{ "_POSIX_MEMLOCK", _SC_MEMLOCK },
	{ "_POSIX_MEMLOCK_RANGE", _SC_MEMLOCK_RANGE },
	{ "_POSIX_MEMORY_PROTECTION", _SC_MEMORY_PROTECTION },
	{ "_POSIX_MESSAGE_PASSING", _SC_MESSAGE_PASSING },
	{ "_POSIX_MONOTONIC_CLOCK", _SC_MONOTONIC_CLOCK },
#if defined(_SC_PRIORITIZED_IO)
	{ "_POSIX_PRIORITIZED_IO", _SC_PRIORITIZED_IO },
#else /* NetBSD */
	{ "_POSIX_PRIORITIZED_IO", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_PRIORITY_SCHEDULING", _SC_PRIORITY_SCHEDULING },
#if defined(_SC_RAW_SOCKETS)
	{ "_POSIX_RAW_SOCKETS", _SC_RAW_SOCKETS },
#else /* NetBSD */
	{ "_POSIX_RAW_SOCKETS", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_READER_WRITER_LOCKS", _SC_READER_WRITER_LOCKS },
#if defined(_SC_REALTIME_SIGNALS)
	{ "_POSIX_REALTIME_SIGNALS", _SC_REALTIME_SIGNALS },
#else /* NetBSD */
	{ "_POSIX_REALTIME_SIGNALS", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_REGEXP", _SC_REGEXP },
	{ "_POSIX_SAVED_IDS", _SC_SAVED_IDS },
	{ "_POSIX_SEMAPHORES", _SC_SEMAPHORES },
#if defined(_SC_SHARED_MEMORY_OBJECTS)
	{ "_POSIX_SHARED_MEMORY_OBJECTS", _SC_SHARED_MEMORY_OBJECTS },
#else /* NetBSD */
	{ "_POSIX_SHARED_MEMORY_OBJECTS", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_SHELL", _SC_SHELL },
	{ "_POSIX_SPAWN", _SC_SPAWN },
	{ "_POSIX_SPIN_LOCKS", _SC_SPIN_LOCKS },
#if defined(_SC_SPORADIC_SERVER)
	{ "_POSIX_SPORADIC_SERVER", _SC_SPORADIC_SERVER },
#else /* NetBSD */
	{ "_POSIX_SPORADIC_SERVER", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_SS_REPL_MAX)
	{ "_POSIX_SS_REPL_MAX", _SC_SS_REPL_MAX },
#else /* FreeBSD */
	{ "_POSIX_SS_REPL_MAX", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_SYNCHRONIZED_IO", _SC_SYNCHRONIZED_IO },
	{ "_POSIX_THREAD_ATTR_STACKADDR", _SC_THREAD_ATTR_STACKADDR },
	{ "_POSIX_THREAD_ATTR_STACKSIZE", _SC_THREAD_ATTR_STACKSIZE },
#if defined(_SC_THREAD_CPUTIME)
	{ "_POSIX_THREAD_CPUTIME", _SC_THREAD_CPUTIME },
#else /* NetBSD */
	{ "_POSIX_THREAD_CPUTIME", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_THREAD_PRIO_INHERIT", _SC_THREAD_PRIO_INHERIT },
	{ "_POSIX_THREAD_PRIO_PROTECT", _SC_THREAD_PRIO_PROTECT },
	{ "_POSIX_THREAD_PRIO_PROTECT", _UNDEFINED_VARIABLE_ },
	{ "_POSIX_THREAD_PRIORITY_SCHEDULING", _SC_THREAD_PRIORITY_SCHEDULING },
	{ "_POSIX_THREAD_PROCESS_SHARED", _SC_THREAD_PROCESS_SHARED },
#if defined(_SC_THREAD_ROBUST_PRIO_INHERIT)
	{ "_POSIX_THREAD_ROBUST_PRIO_INHERIT", _SC_THREAD_ROBUST_PRIO_INHERIT },
#else /* FreeBSD */
	{ "_POSIX_THREAD_ROBUST_PRIO_INHERIT", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_THREAD_ROBUST_PRIO_PROTECT)
	{ "_POSIX_THREAD_ROBUST_PRIO_PROTECT", _SC_THREAD_ROBUST_PRIO_PROTECT },
#else /* FreeBSD */
	{ "_POSIX_THREAD_ROBUST_PRIO_PROTECT", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_THREAD_SAFE_FUNCTIONS", _SC_THREAD_SAFE_FUNCTIONS },
#if defined(_SC_THREAD_SPORADIC_SERVER)
	{ "_POSIX_THREAD_SPORADIC_SERVER", _SC_THREAD_SPORADIC_SERVER },
#else /* NetBSD */
	{ "_POSIX_THREAD_SPORADIC_SERVER", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_THREADS", _SC_THREADS },
#if defined(_SC_TIMEOUTS)
	{ "_POSIX_TIMEOUTS", _SC_TIMEOUTS },
#else /* NetBSD */
	{ "_POSIX_TIMEOUTS", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_TIMERS", _SC_TIMERS },
#if defined(_SC_TRACE)
	{ "_POSIX_TRACE", _SC_TRACE },
#else /* NetBSD */
	{ "_POSIX_TRACE", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_EVENT_FILTER)
	{ "_POSIX_TRACE_EVENT_FILTER", _SC_TRACE_EVENT_FILTER },
#else /* NetBSD */
	{ "_POSIX_TRACE_EVENT_FILTER", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_EVENT_NAME_MAX)
	{ "_POSIX_TRACE_EVENT_NAME_MAX", _SC_TRACE_EVENT_NAME_MAX },
#else /* FreeBSD */
	{ "_POSIX_TRACE_EVENT_NAME_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_INHERIT)
	{ "_POSIX_TRACE_INHERIT", _SC_TRACE_INHERIT },
#else /* NetBSD */
	{ "_POSIX_TRACE_INHERIT", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_LOG)
	{ "_POSIX_TRACE_LOG", _SC_TRACE_LOG },
#else /* NetBSD */
	{ "_POSIX_TRACE_LOG", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_NAME_MAX)
	{ "_POSIX_TRACE_NAME_MAX", _SC_TRACE_NAME_MAX },
#else /* FreeBSD */
	{ "_POSIX_TRACE_NAME_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_SYS_MAX)
	{ "_POSIX_TRACE_SYS_MAX", _SC_TRACE_SYS_MAX },
#else /* FreeBSD */
	{ "_POSIX_TRACE_SYS_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TRACE_USER_EVENT_MAX)
	{ "_POSIX_TRACE_USER_EVENT_MAX", _SC_TRACE_USER_EVENT_MAX },
#else /* FreeBSD */
	{ "_POSIX_TRACE_USER_EVENT_MAX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_TYPED_MEMORY_OBJECTS)
	{ "_POSIX_TYPED_MEMORY_OBJECTS", _SC_TYPED_MEMORY_OBJECTS },
#else /* NetBSD */
	{ "_POSIX_TYPED_MEMORY_OBJECTS", _UNDEFINED_VARIABLE_ },
#endif
	{ "_POSIX_VERSION", _SC_VERSION },
#if defined(_SC_V7_ILP32_OFF32)
	{ "_POSIX_V7_ILP32_OFF32", _SC_V7_ILP32_OFF32 },
#else /* FreeBSD */
	{ "_POSIX_V7_ILP32_OFF32", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_V7_ILP32_OFFBIG)
	{ "_POSIX_V7_ILP32_OFFBIG", _SC_V7_ILP32_OFFBIG },
#else /* FreeBSD */
	{ "_POSIX_V7_ILP32_OFFBIG", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_V7_LP64_OFF64)
	{ "_POSIX_V7_LP64_OFF64", _SC_V7_LP64_OFF64 },
#else /* FreeBSD */
	{ "_POSIX_V7_LP64_OFF64", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_V7_LPBIG_OFFBIG)
	{ "_POSIX_V7_LPBIG_OFFBIG", _SC_V7_LPBIG_OFFBIG },
#else /* FreeBSD */
	{ "_POSIX_V7_LPBIG_OFFBIG", _UNDEFINED_VARIABLE_ },
#endif

	/* POSIX.2 */
	{ "POSIX2_C_BIND", _SC_2_C_BIND },
	{ "POSIX2_C_DEV", _SC_2_C_DEV },
	{ "POSIX2_CHAR_TERM", _SC_2_CHAR_TERM },
	{ "POSIX2_FORT_DEV", _SC_2_FORT_DEV },
	{ "POSIX2_FORT_RUN", _SC_2_FORT_RUN },
	{ "POSIX2_LOCALEDEF", _SC_2_LOCALEDEF },
	{ "POSIX2_PBS", _SC_2_PBS },
	{ "POSIX2_PBS_ACCOUNTING", _SC_2_PBS_ACCOUNTING },
	{ "POSIX2_PBS_CHECKPOINT", _SC_2_PBS_CHECKPOINT },
	{ "POSIX2_PBS_LOCATE", _SC_2_PBS_LOCATE },
	{ "POSIX2_PBS_MESSAGE", _SC_2_PBS_MESSAGE },
	{ "POSIX2_PBS_TRACK", _SC_2_PBS_TRACK },
	{ "POSIX2_SW_DEV", _SC_2_SW_DEV },
	{ "POSIX2_UPE", _SC_2_UPE },
	{ "POSIX2_VERSION", _SC_2_VERSION },

	/* X/Open */
#if defined(_SC_XOPEN_CRYPT)
	{ "_XOPEN_CRYPT", _SC_XOPEN_CRYPT },
#else /* NetBSD */
	{ "_XOPEN_CRYPT", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_XOPEN_ENH_I18N)
	{ "_XOPEN_ENH_I18N", _SC_XOPEN_ENH_I18N },
#else /* NetBSD */
	{ "_XOPEN_ENH_I18N", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_XOPEN_REALTIME)
	{ "_XOPEN_REALTIME", _SC_XOPEN_REALTIME },
#else /* NetBSD */
	{ "_XOPEN_REALTIME", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_XOPEN_REALTIME_THREADS)
	{ "_XOPEN_REALTIME_THREADS", _SC_XOPEN_REALTIME_THREADS },
#else /* NetBSD */
	{ "_XOPEN_REALTIME_THREADS", _UNDEFINED_VARIABLE_ },
#endif
	{ "_XOPEN_SHM", _SC_XOPEN_SHM },
#if defined(_SC_XOPEN_STREAMS)
	{ "_XOPEN_STREAMS", _SC_XOPEN_STREAMS },
#else /* NetBSD */
	{ "_XOPEN_STREAMS", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_XOPEN_UNIX)
	{ "_XOPEN_UNIX", _SC_XOPEN_UNIX },
#else /* NetBSD */
	{ "_XOPEN_UNIX", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_XOPEN_UUCP)
	{ "_XOPEN_UUCP", _SC_XOPEN_UUCP },
#else /* glibc */
	{ "_XOPEN_UUCP", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_XOPEN_VERSION)
	{ "_XOPEN_VERSION", _SC_XOPEN_VERSION },
#else /* NetBSD */
	{ "_XOPEN_VERSION", _UNDEFINED_VARIABLE_ },
#endif
#if defined(_SC_NPROCESSORS_ONLN)
	{ "_NPROCESSORS_ONLN", _SC_NPROCESSORS_ONLN },
#else /* only available on Linux, Solaris, AIX, Mac OS X */
	{ "_NPROCESSORS_ONLN", _UNDEFINED_VARIABLE_ },
#endif

	{ "POSIX2_LINE_MAX", SHIMMY_POSIX2_LINE_MAX },
	{ "INT_MAX", SHIMMY_INT_MAX },
#if defined(__LONG_WIDTH__)
	{ "LONG_BIT", SHIMMY_LONG_BIT }
#endif
};


const int sysvar_count = sizeof(sysconf_vars) / sizeof(sysconf_vars[0]);


const var_map_t confstr_vars[] = {
	{ "PATH", _CS_PATH },
	/* Work around GNU autogen brain damage. */
	{ "CS_PATH", _CS_PATH },
#if defined(_CS_V7_ENV)
	{ "V7_ENV", _CS_V7_ENV }
#else
	{ "V7_ENV", _UNDEFINED_VARIABLE_ }
#endif
};


const int confstr_count = sizeof(confstr_vars) / sizeof(confstr_vars[0]);


const var_map_t limit_vars[] = {
	{ "_POSIX_AIO_LISTIO_MAX", _POSIX_AIO_LISTIO_MAX },
	{ "_POSIX_AIO_MAX", _POSIX_AIO_MAX },
	{ "_POSIX_ARG_MAX", _POSIX_ARG_MAX },
	{ "_POSIX_CHILD_MAX", _POSIX_CHILD_MAX },
	{ "_POSIX_DELAYTIMER_MAX", _POSIX_DELAYTIMER_MAX },
	{ "_POSIX_HOST_NAME_MAX", _POSIX_HOST_NAME_MAX },
	{ "_POSIX_LINK_MAX", _POSIX_LINK_MAX },
	{ "_POSIX_LOGIN_NAME_MAX", _POSIX_LOGIN_NAME_MAX },
	{ "_POSIX_MAX_CANON", _POSIX_MAX_CANON },
	{ "_POSIX_MAX_INPUT", _POSIX_MAX_INPUT },
	{ "_POSIX_NAME_MAX", _POSIX_NAME_MAX },
	{ "_POSIX_NGROUPS_MAX", _POSIX_NGROUPS_MAX },
	{ "_POSIX_OPEN_MAX", _POSIX_OPEN_MAX },
	{ "_POSIX_PATH_MAX", _POSIX_PATH_MAX },
	{ "_POSIX_PIPE_BUF", _POSIX_PIPE_BUF },
	{ "_POSIX_RE_DUP_MAX", _POSIX_RE_DUP_MAX },
	{ "_POSIX_RTSIG_MAX", _POSIX_RTSIG_MAX },
	{ "_POSIX_SEM_NSEMS_MAX", _POSIX_SEM_NSEMS_MAX },
	{ "_POSIX_SEM_VALUE_MAX", _POSIX_SEM_VALUE_MAX },
	{ "_POSIX_SIGQUEUE_MAX", _POSIX_SIGQUEUE_MAX },
	{ "_POSIX_SSIZE_MAX", _POSIX_SSIZE_MAX },
	{ "_POSIX_STREAM_MAX", _POSIX_STREAM_MAX },
	{ "_POSIX_SYMLINK_MAX", _POSIX_SYMLINK_MAX },
	{ "_POSIX_SYMLOOP_MAX", _POSIX_SYMLOOP_MAX },
	{ "_POSIX_THREAD_DESTRUCTOR_ITERATIONS",
		_POSIX_THREAD_DESTRUCTOR_ITERATIONS },
	{ "_POSIX_THREAD_KEYS_MAX", _POSIX_THREAD_KEYS_MAX },
	{ "_POSIX_THREAD_THREADS_MAX", _POSIX_THREAD_THREADS_MAX },
	{ "_POSIX_TIMER_MAX", _POSIX_TIMER_MAX },
	{ "_POSIX_TTY_NAME_MAX", _POSIX_TTY_NAME_MAX },
	{ "_POSIX_TZNAME_MAX", _POSIX_TZNAME_MAX },
	{ "_POSIX2_BC_BASE_MAX", _POSIX2_BC_BASE_MAX },
	{ "_POSIX2_BC_DIM_MAX", _POSIX2_BC_DIM_MAX },
	{ "_POSIX2_BC_SCALE_MAX", _POSIX2_BC_SCALE_MAX },
	{ "_POSIX2_BC_STRING_MAX", _POSIX2_BC_STRING_MAX },
	{ "_POSIX2_CHARCLASS_NAME_MAX", _POSIX2_CHARCLASS_NAME_MAX },
	{ "_POSIX2_COLL_WEIGHTS_MAX", _POSIX2_COLL_WEIGHTS_MAX },
	{ "_POSIX2_EXPR_NEST_MAX", _POSIX2_EXPR_NEST_MAX },
	{ "_POSIX2_LINE_MAX", _POSIX2_LINE_MAX },
	{ "_POSIX2_RE_DUP_MAX", _POSIX2_RE_DUP_MAX }
};


const int limit_count = sizeof(limit_vars) / sizeof(limit_vars[0]);


const var_map_t path_vars[] = {
	{ "FILESIZEBITS", _PC_FILESIZEBITS },
	{ "LINK_MAX", _PC_LINK_MAX },
	{ "MAX_CANON", _PC_MAX_CANON },
	{ "MAX_INPUT", _PC_MAX_INPUT },
	{ "NAME_MAX", _PC_NAME_MAX },
	{ "PATH_MAX", _PC_PATH_MAX },
	{ "PIPE_BUF", _PC_PIPE_BUF },
	{ "POSIX2_SYMLINKS", _PC_2_SYMLINKS },
	{ "POSIX_ALLOC_SIZE_MIN", _PC_ALLOC_SIZE_MIN },
	{ "POSIX_REC_INCR_XFER_SIZE", _PC_REC_INCR_XFER_SIZE },
	{ "POSIX_REC_MAX_XFER_SIZE", _PC_REC_MAX_XFER_SIZE },
	{ "POSIX_REC_MIN_XFER_SIZE", _PC_REC_MIN_XFER_SIZE },
	{ "POSIX_REC_XFER_ALIGN", _PC_REC_XFER_ALIGN },
	{ "SYMLINK_MAX", _PC_SYMLINK_MAX }
};


const int path_count = sizeof(path_vars) / sizeof (path_vars[0]);


int do_system_var(char *environment, char *system_var)
{
	int var;

	if(environment && strlen(environment) != 0)
	{
		fprintf(stderr, "Unsupported environment: %s\n", environment);
		return EINVAL;
	}

	for(var = 0; var < sysvar_count; var++)
	{
		long int val;

		if(strcasecmp(sysconf_vars[var].variable, system_var) != 0)
			continue;

		if(sysconf_vars[var].value == _UNDEFINED_VARIABLE_)
		{
			printf("undefined\n");
			return 0;
		}

		switch(sysconf_vars[var].value)
		{
		case SHIMMY_POSIX2_LINE_MAX:
			val = LINE_MAX;
			break;
		case SHIMMY_INT_MAX:
			val = INT_MAX;
			break;
#if defined(__LONG_WIDTH__)
		case SHIMMY_LONG_BIT:
			val = __LONG_WIDTH__;
			break;
#endif
		default:
			errno = 0;

			val = sysconf(sysconf_vars[var].value);
			if(errno != 0)
			{
			/* We know that this is a valid symbol, so don't break
			 * expectations (see _POSIX_TRACE_LOG in FreeBSD).
			 * 
			 * Note that POSIX states explicitly that results are
			 * already unspecified if the value is dependent on an
			 * unsupported POSIX option.  It also indicates that
			 * *all* variables defined for sysconf *must* return
			 * a result in getconf.  So, I feel this is the
			 * strictest way to conform.
			 */
				printf("undefined\n");
				return 0;
			}
		}

		printf("%ld\n", val);
		return 0;
	}

	for(var = 0; var < limit_count; var++)
	{
		if(strcasecmp(limit_vars[var].variable, system_var) != 0)
			continue;

		printf("%d\n", limit_vars[var].value);
		return 0;
	}

	for(var = 0; var < confstr_count; var++)
	{
		size_t str_len;
		char *str;

		if(strcasecmp(confstr_vars[var].variable, system_var) != 0)
			continue;

		if(confstr_vars[var].value == _UNDEFINED_VARIABLE_)
		{
			printf("undefined\n");
			return 0;
		}

		errno = 0;

		str_len = confstr(confstr_vars[var].value, NULL, 0);
		if(errno != 0 || str_len == 0)
		{
			/* We know that this is a valid symbol, so don't break
			 * expectations (see above comment for sysconf).
			 */
			printf("undefined\n");
			return 0;
		}

		str = malloc(str_len);
		confstr(confstr_vars[var].value, str, str_len);
		printf("%s\n", str);
		free(str);

		return 0;
	}

	fprintf(stderr, "%s: not a recognised system variable\n", system_var);
	return EINVAL;
}


int do_path_var(char *environment, char *path_var, char *pathname)
{
	int var;

	for(var = 0; var < path_count; var++)
	{
		long int result;

		if(strcasecmp(path_vars[var].variable, path_var) != 0)
		{
			continue;
		}

		if(path_vars[var].value == _UNDEFINED_VARIABLE_)
		{
			printf("undefined\n");
			return 0;
		}

		errno = 0;

#ifdef __linux__
		switch(path_vars[var].value)
		{
		case _PC_REC_XFER_ALIGN:
		case _PC_ALLOC_SIZE_MIN:
		{
			struct statvfs vfsres;
			if(statvfs(pathname, &vfsres) == 0)
			{
				result = vfsres.f_frsize;
				printf("%ld\n", result);
				return 0;
			}
			break;
		}
		case _PC_REC_MIN_XFER_SIZE:
		{
			struct statvfs vfsres;
			if(statvfs(pathname, &vfsres) == 0)
			{
				result = vfsres.f_bsize;
				printf("%ld\n", result);
				return 0;
			}
			break;
		}
		case _PC_NAME_MAX:
		{
			struct statfs fsres;
			if(statfs(pathname, &fsres) == 0)
			{
				result = fsres.f_namelen;
				printf("%ld\n", result);
				return 0;
			}
			break;
		}
		}
#endif
		result = pathconf(pathname, path_vars[var].value);
		if(errno != 0)
		{
		/* We know that this is a valid symbol, so don't break
		 * expectations.
		 */
			printf("undefined\n");
			return 0;
		}

		printf("%ld\n", result);
		return 0;
	}

	fprintf(stderr, "%s: not a recognised system variable\n", path_var);
	return EINVAL;
}


int main(int argc, char *argv[])
{
	/* The envrionment passed by -v, if any.  Ignored for now. */
	char *environment = NULL;
	/* An error occurred during argument parsing */
	bool arg_error = false;
	/* The current argument being parsed */
	int arg;

	while((arg = getopt(argc, argv, ":v:")) != -1)
	{
		switch(arg)
		{
		case 'v':
			environment = optarg;
			break;
		case ':':
			fprintf(stderr, "required argument to '-%c' missing\n",
				optopt);
			arg_error = true;
			break;
		case '?':
		default:
			fprintf(stderr, "unrecognised option: '-%c'\n", optopt);
			arg_error = true;
			break;
		}
	}

	switch(argc - optind)
	{
	case 1:
		return do_system_var(environment, argv[optind++]);
	case 2:
		return do_path_var(environment, argv[optind], argv[optind+1]);
	default:
		/* just show usage. */
		arg_error = true;
		break;
	}

	if(arg_error)
	{
		fprintf(stderr, "usage:\n");
		fprintf(stderr, "getconf [-v specification] system_var\n");
		fprintf(stderr,
			"getconf [-v specification] path_var pathname\n");
		fprintf(stderr, "\n\tRetrieve system configuration value.\n");
		return EINVAL;
	}

	return 0;
}
