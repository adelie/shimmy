======================
 Changelog for Shimmy
======================
:Author:
  * **A. Wilcox**, documentation writer
  * **Contributors**, code
:Copyright:
  © 2016-2024 Adélie Linux and contributors.



0.8 (2024-11-19)
================

getconf(1)
----------

* Add `LONG_BIT` variable for compatibility with elfutils.



0.7 (2022-05-24)
================

hostname(1)
-----------

* Add `hostname(1)` utility.



0.6 (2019-03-15)
================

getconf(1)
----------

* Add `_NPROCESSORS_ONLN` variable on supported systems.

* POSIX2 variables are no longer prefixed with an underscore (_).

* Support more path variables on Linux systems.



0.5 (2018-08-21)
================

getconf(1)
----------

* Added support for path variables.



0.4 (2018-03-26)
================

Initial public release.
