===================
 README for Shimmy
===================
:Authors:
  * **A. Wilcox**, maintainer, principal developer
:Status:
  Beta
:Copyright:
  © 2016-2024 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains code and documentation for Shimmy, a small set of
utilities designed to bring stricter POSIX® compliance to open-source operating
environments.  It is designed mainly for use on Adélie Linux, but it is also
occasionally tested on Gentoo Linux, FreeBSD, OpenBSD, and Mac OS, among others.

Note that for ideal compliance, you need to perform any *one* of the following
tasks:

* Install these utilties to a path listed in your system's _CS_PATH (typically,
  this is only /bin and /usr/bin).

* *or* ensure that the directory where Shimmy is installed comes first in PATH,
  and that getconf(1) PATH returns the directory as well, typically by patching
  it.


License
```````

As the Adélie Linux project is an open-source Linux distribution, this package
is distributed under the same NCSA open source license as the distribution.


Changes
```````

Any changes to this repository must be reviewed before being pushed to the
current branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Included utilities
==================

This section contains information about the utilities included with the Shimmy
package.


getconf(1)
``````````

The getconf(1) utility returns configuration information about the presently
running operating environment using sysconf(3), confstr(3), and fpathconf(3).

With the exception of OpenBSD, almost all tested targets had at least one
issue with strict POSIX® conformance.

* FreeBSD returned "Invalid argument" on _POSIX_TRACE_LOG, instead of the
  expected result "undefined" or "-1".

* glibc returned "Unrecognised variable" for V7_ENV when it has a defined value
  in glibc's confstr(3) implementation.  It also considers _POSIX_MAX_INPUT and
  MAX_INPUT as the same variable, which is incorrect (the former being the total
  maximum implementation value, and the latter being the maximum value for a
  specified terminal file).

* musl does not ship a getconf; however, Alpine Linux ships a getconf utility.
  It is heavily obsolescent (being based on 1997 NetBSD code).  It does not
  recognise _XOPEN_* variables nor does it recognise _-prefixed POSIX2
  variables.

* NetBSD does not recognise _-prefixed POSIX2 variables.


hostname(1)
```````````

The hostname(1) utility returns the current host and/or domain name for the
presently running operating environment.  It additionally allows a superuser
to set the host and/or domain name.

This utility is not present in POSIX®, but is needed for setting the host
name on Adélie system startup, and is generally useful.

This implementation has been tested on Linux and Darwin.  It has most of the
Linux net-tools short flags, but does not presently support long flags.


localedef(1)
````````````

The localedef(1) utilty returns 3, denoting that the implementation does not
support the creation of new locales.
